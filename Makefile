all: set

# 0.0 shouldn't clobber any released builds
TAG =0.50
PREFIX = remotejob/nginx-cluster

controller: controller.go
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-w' -o controller ./controller.go

container:  controller
	docker build -t $(PREFIX):$(TAG)  .

push: container
	docker push $(PREFIX):$(TAG)

set: push 
	 #ssh root@159.203.107.223 kubectl set image deployment/nginx-cluster nginx-cluster=$(PREFIX):$(TAG)
	 ssh root@159.203.107.223 kubectl rolling-update nginx-cluster --image=$(PREFIX):$(TAG)

clean:
	docker rmi -f $(PREFIX):$(TAG) || true
