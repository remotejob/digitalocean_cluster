FROM nginx:alpine
COPY controller /
COPY simpledefault.conf /etc/nginx/nginx.conf
CMD ["/controller"]
#COPY default.conf /etc/nginx/conf.d/default.conf
